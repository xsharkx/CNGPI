﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CNGPI_GameMachine
{
    public partial class MachineSlot : UserControl
    {
        public MachineSlot()
        {
            InitializeComponent();
        }

        public int CurrentSlot { get; set; } = 1;

        public EventHandler StartGameEvent { get; set; }

        public EventHandler AlertEvent { get; set; }

        public EventHandler<GameOverEventArgs> GameOverEvent { get; set; }


        private void MachineSlot_Load(object sender, EventArgs e)
        {
            this.groupBox1.Text = $"当前P位：{CurrentSlot}";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            StartGameEvent?.Invoke(this, e);
        }

        private void btnAlert_Click(object sender, EventArgs e)
        {
            AlertEvent?.Invoke(this, e);
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            GameOverEvent?.Invoke(this, new GameOverEventArgs() { Amount = 100 });
        }

        private void btnFinish2_Click(object sender, EventArgs e)
        {
            GameOverEvent?.Invoke(this, new GameOverEventArgs() { Amount = 0 });
        }
    }

    public class GameOverEventArgs : EventArgs
    {
        public int Amount { get; set; }
    }
}
