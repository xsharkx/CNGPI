
#ifndef __MAIN_H__
#define __MAIN_H__

#include <stm32f10x_conf.h>

// 开关全局中断的宏
#define ENABLE_INT()	__set_PRIMASK(0)	// 使能全局中断
#define DISABLE_INT()	__set_PRIMASK(1)	// 禁止全局中断

// 自定义分区flash
#define APP0_ADDR 0x08001800 //应用程序1首地址定义
#define APP1_ADDR 0x08007C00 //应用程序2首地址定义


#define SoftVer     1       // fix bug: 1.frist
#define HardVer     100

#define CONNECT_VER     132

#define APP_NAME            "KaDingCheApp_ych"
#define APP_TITLE           "KaDingCheApp_ych"
#define APP_DEVICE_TYPE     "KaDingChe_ych"
#define DEVICE_PRODUCT_ID   (0x00010004)
#define DEVICE_TYPE         (0x0001)

// menu
#define MENU_TYPE_ENUM      (1)
#define MENU_TYPE_VALUE     (2)
#define MENU_TYPE_STRING    (3)


#endif  //  __MAIN_H__
