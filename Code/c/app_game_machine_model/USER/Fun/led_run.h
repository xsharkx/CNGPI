
#ifndef __LEN_RUN_H__
#define __LEN_RUN_H__

#include <stm32f10x_conf.h>

void led_run_init(void);
void led_run_set(uint8_t val);

#endif  //  __LEN_RUN_H__
