
#ifndef _USART1_DMA_H_
#define _USART1_DMA_H_

#include "stm32f10x_conf.h"
#include "main.h"

// #define USART1_MY_TX_LEN      200     //定义最大发送字节数
#define USART1_MY_TX_LEN      128     //定义最大发送字节数

/**
 * @brief 
 * 升级块大小, max: 1K, 不使用升级,填0
 * 根据程序大小确定大小!!! 越大升级越快,占用空间越大.
 */
#define USART1_DMA_UPDATE_LEN  (0)
#define USART1_DMA_RX_LEN      (256 + USART1_DMA_UPDATE_LEN)    //定义最大接收字节数,不考虑升级

#define USART1_DMA_TX_LEN      (USART1_MY_TX_LEN*2)     //定义最大发送字节数
#define EN_USART1_DMA_RX        1       //使能（1）/禁止（0）串口1接收


void uart1_dma_init(uint32_t bound);

/**
 * @brief 
 * 此函数在main函数的while函数里面调用。
 */
void uart1_dma_get_data_check(void);

// void uart1_dma_send_data_push(uint8_t data);
void uart1_dma_send_data_push(uint8_t *data, uint16_t data_len);

#endif // _USART1_DMA_H_
