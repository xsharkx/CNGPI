
#ifndef __KEY_H__
#define __KEY_H__

#define KEY_NFC                 0
#define KEY_KARTING_POWER       1
#define KEY_STUDY               2

#include "stm32f10x_conf.h"


void key_init(void);

void key_clear_all(void);
uint8_t key_state(uint8_t no);
uint8_t key_press(uint8_t no);

void key_check(void);

uint8_t key_nfc(void);

#endif  //  __KEY_H__
