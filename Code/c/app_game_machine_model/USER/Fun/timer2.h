
#ifndef __TIMER2_H__
#define __TIMER2_H__

#include <stm32f10x_conf.h>

void timer2_init(uint32_t period, uint32_t prescaler);

void timer2_100ms_clean(void);
uint8_t timer2_100ms_sta_get(void);

void timer2_500ms_clean(void);
uint8_t timer2_500ms_sta_get(void);

void timer2_100ms_update_clean(void);
uint8_t timer2_100ms_update_sta_get(void);

// game
void timer2_1ms_Game_clean(void);
uint8_t timer2_1ms_Game_sta_get(void);

void timer2_100ms_Game_clean(void);
uint8_t timer2_100ms_Game_sta_get(void);

#endif  //  __TIMER2_H__
