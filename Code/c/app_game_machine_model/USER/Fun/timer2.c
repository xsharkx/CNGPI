
#include "timer2.h"
#include "key.h"

uint8_t timer2_2ms;

uint8_t timer2_100ms;
uint8_t timer2_100ms_sta;

uint8_t timer2_500ms;
uint8_t timer2_500ms_sta;

uint8_t timer2_100ms_update;
uint8_t timer2_100ms_update_sta;

uint8_t timer2_1ms_Game_sta;

uint8_t timer2_100ms_Game;
uint8_t timer2_100ms_Game_sta;


void timer2_init(uint32_t period, uint32_t prescaler)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseStructure.TIM_Period = period - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = prescaler - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	TIM_Cmd(TIM2, ENABLE);
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

	timer2_2ms = 0;

	timer2_100ms = 0;
	timer2_100ms_sta = 0;

	timer2_500ms = 0;
	timer2_500ms_sta = 0;
	
	timer2_100ms_update = 0;
	timer2_100ms_update_sta = 0;
	
	timer2_100ms_update = 0;
	timer2_100ms_update_sta = 0;

	timer2_1ms_Game_sta = 0;
	
	timer2_100ms_Game = 0;
	timer2_100ms_Game_sta = 0;
}

void TIM2_IRQHandler(void)
{
	if(TIM_GetFlagStatus(TIM2, TIM_IT_Update) == SET)
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

		timer2_1ms_Game_sta = 1;

		if(++timer2_2ms >= 2)
		{
			timer2_2ms = 0;

			key_check();

			if (++timer2_100ms >= 50)
			{
				timer2_100ms = 0;
				timer2_100ms_sta = 1;

				if (++timer2_500ms >= 5)
				{
					timer2_500ms = 0;
					timer2_500ms_sta = 1;
				}
			}

			if(++timer2_100ms_update >= 50)
			{
				timer2_100ms_update = 0;
				timer2_100ms_update_sta = 1;
			}

			if(++timer2_100ms_Game >= 50)
			{
				timer2_100ms_Game = 0;
				timer2_100ms_Game_sta = 1;
			}
		}
	}
}


void timer2_100ms_clean(void)
{
	timer2_100ms = 0;
	timer2_100ms_sta = 0;
}

uint8_t timer2_100ms_sta_get(void)
{
	if(timer2_100ms_sta)
	{
		timer2_100ms_sta = 0;
		return 1;
	}

	return 0;
}

void timer2_500ms_clean(void)
{
	timer2_500ms = 0;
	timer2_500ms_sta = 0;
}

uint8_t timer2_500ms_sta_get(void)
{
	if(timer2_500ms_sta)
	{
		timer2_500ms_sta = 0;
		return 1;
	}

	return 0;
}

void timer2_100ms_update_clean(void)
{
	timer2_100ms_update = 0;
	timer2_100ms_update_sta = 0;
}

uint8_t timer2_100ms_update_sta_get(void)
{
	if(timer2_100ms_update_sta)
	{
		timer2_100ms_update_sta = 0;
		return 1;
	}

	return 0;
}

void timer2_1ms_Game_clean(void)
{
	timer2_1ms_Game_sta = 0;
}

uint8_t timer2_1ms_Game_sta_get(void)
{
	if(timer2_1ms_Game_sta)
	{
		timer2_1ms_Game_sta = 0;
		return 1;
	}

	return 0;
}

void timer2_100ms_Game_clean(void)
{
	timer2_100ms_Game = 0;
	timer2_100ms_Game_sta = 0;
}

uint8_t timer2_100ms_Game_sta_get(void)
{
	if(timer2_100ms_Game_sta)
	{
		timer2_100ms_Game_sta = 0;
		return 1;
	}

	return 0;
}

