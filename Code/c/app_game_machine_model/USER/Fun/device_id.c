
/**
 * @file device_id.c
 * @author Model_Hui (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright Copyright (c) 2023  YouCaiHua Information Technology Co., Ltd
 * 
 */

#include "device_id.h"
#include <stdio.h>

// STM32每个系列都会有唯一的一个芯片序列号(96位bit):
// STM32F10X 的地址是 0x1FFFF7E8
// STM32F20X 的地址是 0x1FFF7A10
// STM32F30X 的地址是 0x1FFFF7AC
// STM32F40X 的地址是 0x1FFF7A10
// STM32L1XX 的地址是 0x1FF80050

uint8_t device_id_buff[12];

void device_id_init(void)
{
	uint8_t i;
	for(i = 0; i < sizeof(device_id_buff); i++)
	{
		device_id_buff[i] = *(uint32_t*)(0x1FFFF7E8 + i);
	}
}

void device_id_show(void)
{
	uint8_t i;

	printf("device_id: ");
	for(i = 0; i < sizeof(device_id_buff); i++)
	{
		// printf("device_id_buff[%d] = %02X\r\n", i, device_id_buff[i]);
		printf("%02X", device_id_buff[i]);
	}
	printf("\r\n");
}



