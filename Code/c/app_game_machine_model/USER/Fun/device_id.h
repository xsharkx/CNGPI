
#ifndef _DEVICE_ID_H_
#define _DEVICE_ID_H_

#include "stm32f10x_conf.h"

// extern uint32_t device_id_buff[3];
extern uint8_t device_id_buff[12];

void device_id_init(void);
void device_id_show(void);

#endif  //  _DEVICE_ID_H_
