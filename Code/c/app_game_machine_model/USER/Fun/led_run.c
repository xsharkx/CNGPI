
/**
 * @file led_run.c
 * @author Model_Hui (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright Copyright (c) 2023  YouCaiHua Information Technology Co., Ltd
 * 
 */

#include "led_run.h"

#define LEN_RUN_NUM (GPIO_Pin_0)
#define LEN_RUN_NUM1 (GPIO_Pin_7)


void led_run_init(void)
{
	// GPIO�˿�����
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LEN_RUN_NUM1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LEN_RUN_NUM;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void led_run_set(uint8_t val)
{
    if(val)
    {
        val = 1;
    }
    GPIO_WriteBit(GPIOA, LEN_RUN_NUM1, (BitAction)val);
    GPIO_WriteBit(GPIOB, LEN_RUN_NUM, (BitAction)val);
}
