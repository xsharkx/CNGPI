
#include "key.h"

uint16_t key_old, key_down;


void key_init(void)
{
	// GPIO�˿�����
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_6 | GPIO_Pin_11;
	// GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

    key_old = 0;
	key_down = 0;

    key_clear_all();
}

void key_check(void)
{
    uint8_t c1, k1, k2;
    
    c1 = 0 ;
	k2 = 0 ;

	do
	{
		k1 = (
          (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4) & 0x01) |
          ((GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6) & 0x01) << 1) |
          ((GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_11) & 0x01) << 2)
          );
		
		if ( k1 == k2 )
		{
			c1 ++ ;
		}
		else
		{
			c1 = 0 ;
			k2 = k1 ;
		}
	}while( c1 < 3 ) ;
	key_down |= ( key_old ^ k1 ) & key_old ;
	key_old = k1 ;
}



void key_clear_all(void)
{
    key_down = 0;
}

uint8_t key_state(uint8_t no)
{
	return (key_old & (1 << no));
}

uint8_t key_press(uint8_t no)
{
	if (key_down & (1 << no))
	{
		key_down &= ~(1 << no);
		return 1;
	}
	return 0 ;
}

uint8_t key_nfc(void)
{
	return key_press( KEY_NFC );
}

