
/**
 * @file usart1_dma.c
 * @author Model_Hui (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright Copyright (c) 2023  YouCaiHua Information Technology Co., Ltd
 * 
 */

#include "usart1_DMA.h"
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "device_id.h"

#include "game_main.h"
// #include "save_flash.h"

#include "uart_data_x.h"

//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数
#pragma import(__use_no_semihosting)
//标准库需要的支持函数
struct __FILE
{
	int handle;
};

FILE __stdout;
//定义_sys_exit()以避免使用半主机模式
void _sys_exit(int x)
{
	x = x;
}
//重定义fputc函数
int fputc(int ch, FILE *f)
{
	USART_SendData(USART1, (uint8_t)ch);
	// while (!USART_GetFlagStatus(USART1, USART_FLAG_TXE));
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);

	return ch;
}

#if EN_USART1_DMA_RX //如果使能了接收
//串口1中断服务程序
//注意,读取USARTx->SR能避免莫名其妙的错误
uint8_t USART1_MY_RX_BUF[USART1_DMA_RX_LEN]; // 接收缓冲,最大 USART1_DMA_RX_LEN 个字节
uint8_t USART1_MY_TX_BUF[USART1_MY_TX_LEN];  // 发送缓冲,最大 USART1_MY_TX_LEN 个字节

uint8_t USART1_DMA_RX_BUF[USART1_DMA_RX_LEN]; // 接收缓冲,最大 USART1_DMA_RX_LEN 个字节
uint8_t USART1_DMA_TX_BUF[USART1_DMA_TX_LEN]; // 发送缓冲,最大 USART1_DMA_TX_LEN*2 个字节

uint16_t uart_get_num;
uint16_t uart_get_over_num;

uint8_t uart_send_finish_flag;
uint16_t uart_wait_send_num;
uint16_t uart_send_num;

uint32_t uart1_recv_counts = 0;
//------------------------------------
// 版本控制
static void USART1_DMA_set_init(void);

void uart1_dma_init(uint32_t bound)
{
	// GPIO端口设置
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	// 参数初始化
	memset(&USART1_DMA_RX_BUF, 0, sizeof(USART1_DMA_RX_BUF));
	memset(&USART1_DMA_TX_BUF, 0, sizeof(USART1_DMA_TX_BUF));
	memset(&USART1_MY_RX_BUF, 0, sizeof(USART1_MY_RX_BUF));
	memset(&USART1_MY_TX_BUF, 0, sizeof(USART1_MY_TX_BUF));
	uart_get_num = 0;
	uart_get_over_num = 0;
	uart_wait_send_num = 0;
	uart_send_num = 0;

	uart1_recv_counts = 0;
	uart_send_finish_flag = 0;

	// 配置
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA, ENABLE); // 使能USART1,GPIOA时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	// GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// USART1 NVIC 配置
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3; //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3; //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; // IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);					//根据指定的参数初始化VIC寄存器

	// USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;										//串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;						//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;							//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;								//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					//收发模式
	USART_Init(USART1, &USART_InitStructure);										//初始化串口1

	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE); //开启中断
	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE); //开dma接收
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE); //开dma接收
	
	USART_Cmd(USART1, ENABLE);					   //使能串口1

	USART_GetFlagStatus(USART1, USART_FLAG_TC);

	USART1_DMA_set_init();
}

#define DMA_USART1_DR_Base      (USART1_BASE + 0x4) //0x40013804
// #define DMA_USART1_DR_Base          0x40013804

static void USART1_DMA_set_init(void)
{
	DMA_InitTypeDef DMA_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;  // DMA通道5中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2; //抢占优先级 0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		  //子优先级2
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			  //使能
	NVIC_Init(&NVIC_InitStructure);

	// Tx DMA CONFIG
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE); // enable DMA1 clock
	DMA_Cmd(DMA1_Channel4, DISABLE);				   // close DMA Channel
	DMA_DeInit(DMA1_Channel4);

	DMA_InitStructure.DMA_PeripheralBaseAddr = DMA_USART1_DR_Base; //(uint32_t)(&USART1->DR)
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)USART1_DMA_TX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = USART1_DMA_TX_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel4, &DMA_InitStructure);
	DMA_ClearFlag(DMA1_FLAG_GL4); // clear all DMA flags
	// DMA_Cmd(DMA1_Channel4,ENABLE); // close DMA Channel
	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE); // open DMA send inttrupt

	// Rx DMA CONFIG
	DMA_Cmd(DMA1_Channel5, DISABLE); //
	DMA_DeInit(DMA1_Channel5);

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&USART1->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)USART1_DMA_RX_BUF;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = USART1_DMA_RX_LEN;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel5, &DMA_InitStructure);
	DMA_ClearFlag(DMA1_FLAG_GL5);
	DMA_Cmd(DMA1_Channel5, ENABLE);
}

///////////////////////uart dma send//////////////////////
void DMA1_Channel4_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA1_FLAG_TC4) == SET)
	{
		DMA_ClearFlag(DMA1_FLAG_GL4);
		DMA_Cmd(DMA1_Channel4, DISABLE);
		uart_send_finish_flag = 0;
	}
}

void uart_dma_send_enable(uint16_t size)
{
    DMA1_Channel4->CNDTR = (uint16_t)size; 
    DMA_Cmd(DMA1_Channel4, ENABLE);       
}

void uart1_dma_send_data_check(void)
{
	uint16_t i;

	if(uart_send_num != uart_wait_send_num && uart_send_finish_flag == 0)
	{
		uart_send_finish_flag = 1;
		i = 0;
		
		while(uart_wait_send_num != uart_send_num)
		{
			USART1_DMA_TX_BUF[i] = USART1_MY_TX_BUF[uart_send_num];

			i++;
			uart_send_num++;
			if(uart_send_num >= USART1_MY_TX_LEN)
			{
				uart_send_num = 0;
			}
		}

		uart_dma_send_enable(i);
	}
}
///////////////////////uart dma send//////////////////////

///////////////////////uart dma recv//////////////////////
void uart1_dma_recv_data(void)
{
	uint16_t i = 0;

	DMA_Cmd(DMA1_Channel5, DISABLE);
	DMA_ClearFlag(DMA1_FLAG_GL5);

	uart1_recv_counts = USART1_DMA_RX_LEN - DMA_GetCurrDataCounter(DMA1_Channel5);

	for (i = 0; i < uart1_recv_counts; i++)
	{
		USART1_MY_RX_BUF[uart_get_num] = USART1_DMA_RX_BUF[i];
		if (++uart_get_num >= USART1_DMA_RX_LEN)
		{
			uart_get_num = 0;
		}
	}

	DMA1_Channel5->CNDTR = USART1_DMA_RX_LEN;
	DMA_Cmd(DMA1_Channel5, ENABLE);
}

void USART1_IRQHandler(void)
{
	if (USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)
	{
		uart1_dma_recv_data();
		// USART_ClearITPendingBit(USART1, USART_IT_IDLE);
		USART_ReceiveData(USART1); // Clear IDLE interrupt flag bit
		USART_ClearFlag(USART1, USART_IT_IDLE);
	}
}

uint8_t uart1_get_data(void)
{
	uint8_t data;

	data = USART1_MY_RX_BUF[uart_get_over_num];
	if (++uart_get_over_num >= USART1_DMA_RX_LEN)
	{
		uart_get_over_num = 0;
	}

	return data;
}
///////////////////////uart dma recv//////////////////////
#endif // EN_USART1_DMA_RX

extern uint8_t *uart_data_x_get_buff_get(void);
extern void uart_data_x_startup_decode(uint16_t data_len);

/**
 * @brief 
 * 此函数在main函数的while函数里面调用。
 */
void uart1_dma_get_data_check(void)
{
	uint16_t len_c;
	uint8_t *buff_add = NULL;

	if(uart1_recv_counts && uart_get_over_num != uart_get_num)
	{
		DISABLE_INT();

		len_c = 0;
		buff_add = uart_data_x_get_buff_get();

		memset(buff_add, 0, sizeof(buff_add));

		while(uart_get_over_num != uart_get_num)
		{
			buff_add[len_c] = uart1_get_data();
			len_c++;
		}
		
		uart1_recv_counts = 0;
		
		ENABLE_INT();

		uart_data_x_startup_decode(len_c);
	}

	uart1_dma_send_data_check();
}

// void uart1_dma_send_data_push(uint8_t data)
// {
// 	USART1_MY_TX_BUF[uart_wait_send_num] = data;
// 	if (++uart_wait_send_num >= USART1_MY_TX_LEN)
// 	{
// 		uart_wait_send_num = 0;
// 	}
// }

void uart1_dma_send_data_push(uint8_t *data, uint16_t data_len)
{
	uint16_t c1;

	for (c1 = 0; c1 < data_len; c1++)
	{
		USART1_MY_TX_BUF[uart_wait_send_num] = *(data + c1);
		if (++uart_wait_send_num >= USART1_MY_TX_LEN)
		{
			uart_wait_send_num = 0;
		}
	}
}
