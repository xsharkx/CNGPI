
/**
 * @file game_main.c
 * @author Model_Hui (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright Copyright (c) 2023  YouCaiHua Information Technology Co., Ltd
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "game_main.h"
#include "main.h"
#include "uart_data_x.h"
#include "key.h"
#include "timer2.h"
#include "device_id.h"


game_main_t game_main_config;
uint8_t game_main_send_buff[128] = {0};

uint8_t device_id_buff_send[16] = {0};

extern void uart_data_x_timer_1ms(void);

//
//
//
static void game_main_time_check_100Ms(void);

static uint16_t game_main_trans_id_get(void);

//
//
//
static void game_main_reply_state(const uint16_t coin_residue,
                                const uint16_t over_time,
                                const uint16_t state);
static void game_main_reply_coin(const uint16_t trans_id);
static void game_main_reply_universal_config_get(const uint16_t result, const uint8_t value);
static void game_main_reply_account(const uint16_t state, 
                                    const uint32_t coin_phy_num_all, 
                                    const uint32_t coin_ele_num_all, 
                                    const uint32_t gift_num_all, 
                                    const uint32_t ticket_num_all);
static void game_main_send_coin_phy(uint16_t trans_id, uint32_t value);
static void game_main_send_game_over(const uint16_t trans_id, 
                                    const uint32_t value, 
                                    const uint8_t out_type,
                                    const uint8_t out_num);
static void game_main_send_fault(uint16_t fault_num, uint8_t *fault_str);
static void game_main_send_device_register(void);
static void game_main_send_lucky(uint16_t trans_id);
static void game_main_send_menu_set(uint16_t value);
static void game_main_send_menu_read(const uint16_t state, 
                                    const uint16_t num, 
                                    const uint8_t menu_type, 
                                    const uint32_t menu_value, 
                                    const uint8_t *menu_str);
static void game_main_send_box_info(void);
static void game_main_send_pay_code(void);
static void game_main_send_update_check(void);
static void game_main_send_update_get_data(const uint32_t offset,
                                            const uint32_t data_size);

static void game_main_uart_init(void);

static void game_main_get_save_data(void)
{
    //-------------------------------------------------
    // 测试使用,
    // stm32系列,芯片唯一ID是字符96bit==12bytes.
    // 此处补充其中4bytes,填满使用.
    device_id_buff_send[0] = 9;
    device_id_buff_send[1] = 5;
    device_id_buff_send[2] = 2;
    device_id_buff_send[3] = 7;
    memcpy((device_id_buff_send + 4), device_id_buff, sizeof(device_id_buff));
    //-------------------------------------------------

    //-------------------------------------------------
    // 此处是测试数据
    game_main_config.coin_residue = 1;
    game_main_config.over_time = 10;
    game_main_config.device_state = 0;
    
    // 设置每局币数
    game_main_config.pay_game_coins = 1;

    // 读取查账数据
    game_main_config.acc_coin_phy_num_all = 1;
    game_main_config.acc_coin_ele_num_all = 1;
    game_main_config.acc_gift_num_all = 1;
    game_main_config.acc_ticket_num_all = 1;

    // 读取实物币数,待发送实物币数
    game_main_config.coin_phy_num = 0;
    game_main_config.coin_phy_send_num = 0;

    // 读取礼品数,读取待发送礼品数
    game_main_config.gift_num = 0;
    game_main_config.gift_num_send = 0;
    // 每次开机,如果有未发送的礼品数,要发送一次.
    if(game_main_config.gift_num_send > 0)
    {
        game_main_config.game_over_flag = 1;
    }
}
//
//
//
void game_main_init(void)
{
    memset(&game_main_config, 0, sizeof(game_main_t));

    // 初始化时,设置好需要的数据
    game_main_get_save_data();

    game_main_uart_init();
    
    game_main_send_device_register();

    game_main_change_sta(GAME_MAIN_STA_IDLE);

    
}

void game_main_run(void)
{
    // timer
    if(timer2_1ms_Game_sta_get())
    {
        uart_data_x_timer_1ms();
    }

    if(timer2_100ms_Game_sta_get())
    {
        game_main_time_check_100Ms();
    }


    // -----------------------------------
    // 模拟---投实物币
    if(key_press(KEY_STUDY))
    {
        if(++game_main_config.coin_phy_num >= 0xFFFFFFFF)
        {
            game_main_config.coin_phy_num = 1;
        }
    }
    // 模拟---投实物币
    // -----------------------------------

    // -----------------------------------
    // 模拟---游戏结束,默认出礼品
    if(key_press(KEY_KARTING_POWER))
    {
        // game_main_send_fault(1);

        // 退礼品
        if(++game_main_config.gift_num >= 0xFFFFFFFF)
        {
            game_main_config.gift_num = 1;
        }
        // 在此处保存 --- 获得礼品数量

        game_main_config.game_over_flag = 1;
    }

    if(game_main_config.game_over_flag == 1)
    {
        game_main_config.game_over_flag = 0;
        
        game_main_config.game_over_try_times = GAME_MAIN_GAME_OVER_TRY_TIMES_MAX;
        game_main_config.game_over_try_times_time = 0;
        game_main_config.game_over_trans_id_send = game_main_trans_id_get();

        if(game_main_config.gift_num > 0)
        {
            game_main_config.gift_num_send += game_main_config.gift_num;
            // 在此处保存 --- 发送的礼品数量

            game_main_config.gift_num = 0;
            // 在此处保存 --- 获得礼品数量
        }

        // game_main_config.game_over_out_type = GAME_MAIN_OUT_TYPE_SCORE;
        // game_main_config.game_over_out_type = GAME_MAIN_OUT_TYPE_TICKET_PHY;
        game_main_config.game_over_out_type = GAME_MAIN_OUT_TYPE_GIFT;
        // game_main_config.game_over_out_type = GAME_MAIN_OUT_TYPE_COIN_PHY;
        // game_main_config.game_over_out_type = GAME_MAIN_OUT_TYPE_TICKET_ELE;
        // game_main_config.game_over_out_type = GAME_MAIN_OUT_TYPE_COIN_ELE;
    }
    // 模拟---游戏结束,默认出礼品
    // -----------------------------------

    if(game_main_config.get_box_info_flag == GAME_MAIN_GET_BOX_INFO_SEND_FLAG_RUN)
    {
        game_main_config.get_box_info_flag = GAME_MAIN_GET_BOX_INFO_SEND_FLAG_TRY;
        game_main_config.get_box_info_time = GAME_MAIN_GET_BOX_INFO_TIME_MAX;

        game_main_send_box_info();
    }

    if(game_main_config.pay_code_flag == GAME_MAIN_PAY_CODE_SEND_FLAG_RUN)
    {
        game_main_config.pay_code_flag = GAME_MAIN_PAY_CODE_SEND_FLAG_TRY;
        game_main_config.pay_code_time = GAME_MAIN_PAY_CODE_TIME_MAX;

        game_main_send_pay_code();
    }
    
    //-----------------------------------
    // 投实物币---是否发送数据
    // 当发送标记位==0,发送缓冲区没有数据,当有实物币投入就启动投币,没有就静止
    // 当发送标记位==0,发送缓冲区有数据就启动发送.
    if(game_main_config.coin_phy_send_flag == GAME_MAIN_COIN_PHY_SEND_FLAG_IDLE)
    {
        if(game_main_config.coin_phy_send_num == 0)
        {
            if(game_main_config.coin_phy_num > 0)
            {
                game_main_config.coin_phy_send_num = game_main_config.coin_phy_num;
                game_main_config.coin_phy_num -= game_main_config.coin_phy_send_num;
                // 更新---实物币数量
                // 更新---待发送的实物币数量

                game_main_config.coin_phy_trans_id_send = game_main_trans_id_get();

                game_main_config.coin_phy_send_flag = GAME_MAIN_COIN_PHY_SEND_FLAG_RUN;
            }
        }
        else
        {
            game_main_config.coin_phy_trans_id_send = game_main_trans_id_get();

            game_main_config.coin_phy_send_flag = GAME_MAIN_COIN_PHY_SEND_FLAG_RUN;
        }
    }

    // 间隔 GAME_MAIN_COIN_PHY_SEND_DELAY_TIME_MAX秒,发送一次数据
    if(game_main_config.coin_phy_send_flag == GAME_MAIN_COIN_PHY_SEND_FLAG_RUN)
    {
        game_main_config.coin_phy_send_flag = GAME_MAIN_COIN_PHY_SEND_FLAG_TRY;

        game_main_send_coin_phy(game_main_config.coin_phy_trans_id_send, game_main_config.coin_phy_send_num);
    }
    // 投币
    //-----------------------------------

    //-----------------------------------
    // 游戏结束
    // 将以每500ms一次的频率发送10次直到有回应,
    // 如10次未回应代表离线将按超时来处理.
    // 建议游戏机将此信息保存起来下次继续执行并在游戏交互界面提示错误与重试.
    if(game_main_config.game_over_try_times > 0)
    {
        if(game_main_config.game_over_try_times_time == 0)
        {
            game_main_config.game_over_try_times_time = GAME_MAIN_GAME_OVER_TRY_TIMES_TIME_MAX;

            game_main_send_game_over(game_main_config.game_over_trans_id_send, 
                                    game_main_config.gift_num_send,
                                    game_main_config.game_over_out_type,
                                    0);
            
            game_main_config.game_over_try_times--;
        }
    }
    // 游戏结束
    //-----------------------------------
}

void game_main_change_sta(uint8_t sta)
{
    game_main_config.state = sta;
    
    // printf("game_main.state = %d\r\n", game_main_config.state);

    // switch (game_main_config.state)
    // {
    // case GAME_MAIN_STA_IDLE:
    //     break;
    // case GAME_MAIN_STA_RUN:
    //     break;
    
    // default:
    //     break;
    // }
}

static void game_main_time_check_100Ms(void)
{
    //------------------------------------------
    // 投实物币---尝试重新发送
    if(game_main_config.coin_phy_send_flag == GAME_MAIN_COIN_PHY_SEND_FLAG_TRY)
    {
        if(game_main_config.coin_phy_send_delay_time > 0)
        {
            game_main_config.coin_phy_send_delay_time--;
        }
        else
        {
            if(game_main_config.coin_phy_send_delay_time == 0)
            {
                game_main_config.coin_phy_send_delay_time = GAME_MAIN_COIN_PHY_SEND_DELAY_TIME_MAX;
                game_main_config.coin_phy_send_flag = GAME_MAIN_COIN_PHY_SEND_FLAG_RUN;
            }
        }
    }

    if(game_main_config.game_over_try_times > 0)
    {
        if(game_main_config.game_over_try_times_time > 0)
        {
            game_main_config.game_over_try_times_time--;
        }
    }

    if(game_main_config.get_box_info_flag == GAME_MAIN_GET_BOX_INFO_SEND_FLAG_TRY)
    {
        if(game_main_config.get_box_info_time > 0)
        {
            game_main_config.get_box_info_time--;
        }
        else
        {
            game_main_config.get_box_info_time = GAME_MAIN_GET_BOX_INFO_TIME_MAX;
            game_main_config.get_box_info_flag = GAME_MAIN_GET_BOX_INFO_SEND_FLAG_RUN;
        }
    }

    if(game_main_config.pay_code_flag == GAME_MAIN_PAY_CODE_SEND_FLAG_TRY)
    {
        if(game_main_config.pay_code_time > 0)
        {
            game_main_config.pay_code_time--;
        }
        else
        {
            game_main_config.pay_code_time = GAME_MAIN_PAY_CODE_TIME_MAX;
            game_main_config.pay_code_flag = GAME_MAIN_PAY_CODE_SEND_FLAG_RUN;
        }
    }
}


static uint16_t game_main_trans_id_get(void)
{
    if(++game_main_config.trans_id >= 0xFFFF)
    {
        game_main_config.trans_id = 1;
    }
    return game_main_config.trans_id;
}

//
//
//
//
//
//
//
//-----uart-----
static void game_main_uart_connet_callback(const uint32_t product_id, const uint8_t *device_id, const uint16_t devoce_type);
static void game_main_uart_state_callback(const uint16_t device_state, const uint8_t signal_value, const uint8_t internet_state);
static void game_main_uart_coin_in_callback(const uint16_t trans_id, const uint16_t coin_num, const uint8_t coin_type, 
									    const uint8_t *member_id, const uint8_t *member_name);
static void game_main_uart_universal_config_get_callback(void);
static void game_main_uart_account_callback(void);
static void game_main_uart_coin_phy_callback(const uint16_t trans_id);
static void game_main_uart_game_over_callback(const uint16_t trans_id, const uint16_t result);
static void game_main_uart_fault_callback(const uint16_t result);
static void game_main_uart_device_register_callback(const uint8_t result, const uint8_t *key_num, const uint8_t *device_id);
static void game_main_uart_lucky_callback(const uint16_t trans_id, const uint8_t play_times, 
                                            const uint8_t *lucky_arry, const uint8_t *member_id);
static void game_main_uart_menu_set_callback(const uint16_t number_id, const uint8_t menu_type, 
                                            const uint32_t menu_value, const uint8_t *menu_str);
static void game_main_uart_menu_read_callback(const uint16_t number_id);
static void game_main_uart_box_info_callback(const uint16_t result, const uint8_t *box_nmber, 
										    const uint8_t net_state, const uint8_t login_state, const uint8_t *shop_name);
static void game_main_uart_pay_code_callback(const uint16_t result, const uint8_t *pay_code);
static void game_main_uart_update_check_callback(const uint8_t update_flag, 
                                                const uint32_t soft_ver, 
                                                const uint8_t internet_state, 
                                                const uint8_t addr_len, 
                                                const uint8_t *data);
static void game_main_uart_update_get_data_callback(const uint32_t offset, 
                                                    const uint32_t data_size,
                                                    const uint8_t *data);

/**
 * @brief 此函数在初始化时调用,注册回调函数.
 */
static void game_main_uart_init(void)
{
    uart_data_x_init();

    CMD_connect_init(CONNECT_VER, 
                    DEVICE_PRODUCT_ID, 
                    device_id_buff_send, 
                    1, 
                    1, 
                    DEVICE_TYPE, 
                    SoftVer, 
                    game_main_uart_connet_callback);
    
    CMD_state_callback_register(game_main_uart_state_callback);
    CMD_coin_in_callback_register(game_main_uart_coin_in_callback);
    CMD_universal_config_get_callback_register(game_main_uart_universal_config_get_callback);
    CMD_account_callback_register(game_main_uart_account_callback);
    CMD_coin_phy_callback_register(game_main_uart_coin_phy_callback);
    CMD_game_over_callback_register(game_main_uart_game_over_callback);
    CMD_fault_callback_register(game_main_uart_fault_callback);
    CMD_device_register_callback_register(game_main_uart_device_register_callback);
    CMD_lucky_callback_register(game_main_uart_lucky_callback);
    CMD_menu_set_callback_register(game_main_uart_menu_set_callback);
    CMD_menu_read_callback_register(game_main_uart_menu_read_callback);
    CMD_box_info_callback_register(game_main_uart_box_info_callback);
    CMD_pay_code_callback_register(game_main_uart_pay_code_callback);
    CMD_update_check_callback_register(game_main_uart_update_check_callback);
    CMD_update_get_data_callback_register(game_main_uart_update_get_data_callback);
}

//
//
//
/**
 * @brief 握手-回调函数
 * 
 * @param product_id 产品id
 * @param device_id 设备唯一id---string
 * @param device_type 设备类型
 */
static void game_main_uart_connet_callback(const uint32_t product_id, const uint8_t *device_id, const uint16_t device_type)
{
    uint8_t product_str[17] = {0};

    printf("connet_callback\r\n");

    memcpy(product_str, device_id, (sizeof(product_str) - 1));

    printf("product_id = %d\r\n", product_id);
    printf("device_id = %s\r\n", product_str);
    printf("device_type = %d\r\n", device_type);

    game_main_config.get_box_info_flag = GAME_MAIN_GET_BOX_INFO_SEND_FLAG_RUN;
    game_main_config.pay_code_flag = GAME_MAIN_PAY_CODE_SEND_FLAG_IDLE;
}

//
//
//
/**
 * @brief 状态同步-回调函数
 * 
 * @param device_state 设备状态
 * @param signal_value 信号强度
 * @param internet_state 联网状态
 */
static void game_main_uart_state_callback(const uint16_t device_state, 
                                        const uint8_t signal_value, 
                                        const uint8_t internet_state)
{
    printf("state_callback\r\n");

    printf("device_state = %d\r\n", device_state);
    printf("signal_value = %d\r\n", signal_value);
    printf("internet_state = %d\r\n", internet_state);
    
    game_main_reply_state(game_main_config.coin_residue, 
                        game_main_config.over_time, 
                        game_main_config.device_state);
}

/**
 * @brief 状态同步-回应
 */
static void game_main_reply_state(const uint16_t coin_residue,
                                    const uint16_t over_time,
                                    const uint16_t state)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    if(game_main_config.state >= GAME_MAIN_STA_RUN)
    {
        game_main_send_buff[0] = 0x03;
    }
    else
    {
        game_main_send_buff[0] = 0;
    }
    
    game_main_send_buff[1] = ((coin_residue >> 8) & 0xFF);
    game_main_send_buff[2] = (coin_residue & 0xFF);
    
    game_main_send_buff[3] = ((over_time >> 8) & 0xFF);
    game_main_send_buff[4] = (over_time & 0xFF);
    
    game_main_send_buff[5] = ((state >> 8) & 0xFF);
    game_main_send_buff[6] = (state & 0xFF);

    CMD_send_status(game_main_send_buff, 7);
}

//
//
//
/**
 * @brief 协议投币-回调函数
 * 
 * @param trans_id 事物id
 * @param coin_num 投币数
 * @param coin_type 投币类型
 * @param member_id 会员id(36 bytes) + 店铺id(8 bytes)---string
 * @param member_name 会员名字---string
 */
static void game_main_uart_coin_in_callback(const uint16_t trans_id, const uint16_t coin_num, 
                                            const uint8_t coin_type, const uint8_t *member_id, 
                                            const uint8_t *member_name)
{
    uint8_t str[37] = {0};

    printf("coin_in_callback\r\n");

    printf("trans_id = %d\r\n", trans_id);

    game_main_config.coin_counts = coin_num;
    printf("coin_num = %d\r\n", coin_num);
    printf("coin_counts = %d\r\n", game_main_config.coin_counts);

    game_main_config.coin_type = coin_type;
    printf("coin_type = %d\r\n", coin_type);
    printf("coin_type = %d\r\n", game_main_config.coin_type);

    memcpy(game_main_config.member_id, member_id, sizeof(game_main_config.member_id));
    memcpy(str, member_id, sizeof(game_main_config.member_id));
    printf("member_id = %s\r\n", member_id);
    printf("member_id = %s\r\n", game_main_config.member_id);
    printf("member_id = %s\r\n", str);

    memcpy(game_main_config.store_id, (member_id + GAME_MAIN_NUMBER_ID_SIZE), sizeof(game_main_config.store_id));
    memset(str, 0, sizeof(str));
    memcpy(str, (member_id + GAME_MAIN_NUMBER_ID_SIZE), sizeof(game_main_config.store_id));
    printf("store_id = %s\r\n", game_main_config.store_id);
    printf("store_id = %s\r\n", str);

    memcpy(game_main_config.member_name, member_name, sizeof(game_main_config.member_name));
    memset(str, 0, sizeof(str));
    memcpy(str, member_name, sizeof(game_main_config.member_name));
    printf("member_name = %s\r\n", member_name);
    printf("member_name = %s\r\n", game_main_config.member_name);
    printf("member_name = %s\r\n", str);

    game_main_reply_coin(trans_id);
}

/**
 * @brief 协议投币-回应
 * @param trans_id 对应的事物id
 */
static void game_main_reply_coin(const uint16_t trans_id)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((trans_id >> 8) & 0xFF);
    game_main_send_buff[1] = (trans_id & 0xFF);
    
    game_main_send_buff[2] = 0;
    game_main_send_buff[3] = 0;

    CMD_send_coin_in_rep(game_main_send_buff, 4);
}

//
//
//
/**
 * @brief 获取通用设置-回调函数
 * 
 */
static void game_main_uart_universal_config_get_callback(void)
{
    uint16_t state = 0;
    printf("universal_config_get_callback\r\n");

    // game_main_config.reply_universal_config_get_flag = 1;
    
    // test
    if(game_main_config.state >= GAME_MAIN_STA_RUN)
    {
        state = 1;
    }
    else
    {
        state = 0;
    }

    game_main_reply_universal_config_get(state, game_main_config.pay_game_coins);
}

/**
 * @brief 获取通用设置-回应
 * 
 * @param result 结果
 * @param value 数值
 */
static void game_main_reply_universal_config_get(const uint16_t result, const uint8_t value)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((result >> 8) & 0xFF);
    game_main_send_buff[1] = (result & 0xFF);

    game_main_send_buff[2] = (value & 0xFF);

    CMD_send_universal_config_get_rep(game_main_send_buff, 3);
}

//
//
//
/**
 * @brief 通用查找-回调函数
 * 
 */
static void game_main_uart_account_callback(void)
{
    printf("account_callback\r\n");

    game_main_reply_account(0,
                            game_main_config.acc_coin_phy_num_all,
                            game_main_config.acc_coin_ele_num_all,
                            game_main_config.acc_gift_num_all,
                            game_main_config.acc_ticket_num_all);
}

/**
 * @brief 通用查账-回应
 * 
 * @param state 设备状态
 * @param coin_phy_num_all 总实物币数量
 * @param coin_ele_num_all 总电子币数量
 * @param gift_num_all 总出礼品数量
 * @param ticket_num_all 总出彩票数量
 */
static void game_main_reply_account(const uint16_t state, 
                                    const uint32_t coin_phy_num_all, 
                                    const uint32_t coin_ele_num_all, 
                                    const uint32_t gift_num_all, 
                                    const uint32_t ticket_num_all)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((state >> 8) & 0xFF);
    game_main_send_buff[1] = (state & 0xFF);

    game_main_send_buff[2] = ((coin_phy_num_all >> 24) & 0xFF);
    game_main_send_buff[3] = ((coin_phy_num_all >> 16) & 0xFF);
    game_main_send_buff[4] = ((coin_phy_num_all >> 8) & 0xFF);
    game_main_send_buff[5] = (coin_phy_num_all & 0xFF);

    game_main_send_buff[6] = ((coin_ele_num_all >> 24) & 0xFF);
    game_main_send_buff[7] = ((coin_ele_num_all >> 16) & 0xFF);
    game_main_send_buff[8] = ((coin_ele_num_all >> 8) & 0xFF);
    game_main_send_buff[9] = (coin_ele_num_all & 0xFF);

    game_main_send_buff[10] = ((gift_num_all >> 24) & 0xFF);
    game_main_send_buff[11] = ((gift_num_all >> 16) & 0xFF);
    game_main_send_buff[12] = ((gift_num_all >> 8) & 0xFF);
    game_main_send_buff[13] = (gift_num_all & 0xFF);

    game_main_send_buff[14] = ((ticket_num_all >> 24) & 0xFF);
    game_main_send_buff[15] = ((ticket_num_all >> 16) & 0xFF);
    game_main_send_buff[16] = ((ticket_num_all >> 8) & 0xFF);
    game_main_send_buff[17] = (ticket_num_all & 0xFF);

    CMD_send_account_rep(game_main_send_buff, 18);
}

//
//
//
/**
 * @brief 上报实物币-回调函数
 * 
 * @param trans_id 事物id
 */
static void game_main_uart_coin_phy_callback(const uint16_t trans_id)
{
    printf("coin_phy_callback\r\n");

    printf("trans_id = %d\r\n", trans_id);

    if(game_main_config.trans_id == game_main_config.coin_phy_trans_id_send)
    {
        game_main_config.coin_phy_send_flag = GAME_MAIN_COIN_PHY_SEND_FLAG_IDLE;

        game_main_config.coin_phy_send_num = 0;
        // 更新---待发送的实物币数量
    }
}

/**
 * @brief 上报实物币-发送
 * @param trans_id 事物id
 * @param value 实物币数量
 */
static void game_main_send_coin_phy(uint16_t trans_id, uint32_t value)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((trans_id >> 8) & 0xff);
    game_main_send_buff[1] = (trans_id & 0xff);

    game_main_send_buff[2] = ((value >> 24) & 0xff);
    game_main_send_buff[3] = ((value >> 16) & 0xff);
    game_main_send_buff[4] = ((value >> 8) & 0xff);
    game_main_send_buff[5] = (value & 0xff);

    CMD_send_coin_phy(game_main_send_buff, 6);
}

//
//
//
/**
 * @brief 游戏结束-回调函数
 * 
 * @param trans_id 事物id
 * @param result 结果
 */
static void game_main_uart_game_over_callback(const uint16_t trans_id, const uint16_t result)
{
    printf("game_over_callback\r\n");

    printf("trans_id = %d\r\n", trans_id);
    printf("result = %d\r\n", result);

    if(trans_id == game_main_config.game_over_trans_id_send)
    {
        if(result == 0)
        {
            // game_main_config.game_over_try_times = 0;
            game_main_config.gift_num_send = 0;
            // 在此处保存 --- 发送的礼品数量
        }
    }
}

/**
 * @brief 游戏结束-发送
 * @param trans_id 事物id
 * @param value 出奖数量
 * @param out_type 出奖类型
 * @param out_num 出奖位置,默认0
 */
static void game_main_send_game_over(const uint16_t trans_id, 
                                    const uint32_t value, 
                                    const uint8_t out_type,
                                    const uint8_t out_num)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((trans_id >> 8) & 0xff);
    game_main_send_buff[1] = (trans_id & 0xff);

    game_main_send_buff[2] = ((value >> 24) & 0xff);
    game_main_send_buff[3] = ((value >> 16) & 0xff);
    game_main_send_buff[4] = ((value >> 8) & 0xff);
    game_main_send_buff[5] = (value & 0xff);

    game_main_send_buff[6] = out_type;
    game_main_send_buff[7] = out_num;
	
    memcpy((game_main_send_buff + 8), game_main_config.member_id, GAME_MAIN_NUMBER_ID_SIZE);
    
    memcpy((game_main_send_buff + 8 + GAME_MAIN_NUMBER_ID_SIZE), game_main_config.store_id, GAME_MAIN_STORE_SIZE);

    CMD_send_game_over(game_main_send_buff, 52);
}

//
//
//
/**
 * @brief 上报故障-回调函数-----
 * PS:此功能需要配合 游志云开发者平台使用,具体请咨询对接人员.
 * 游志云开发者平台: http://yzy.gzych.vip/yzyDev/account/login
 * 
 * @param result 上报故障结果
 */
static void game_main_uart_fault_callback(const uint16_t result)
{
    printf("fault_callback\r\n");

    printf("result = %d\r\n", result);

    if(result == 0)
    {
        //
    }
}

/**
 * @brief 上报故障-发送
 * @param fault_num 自定义故障编号---配合游志云开发者平台
 * @param fault_str 故障简述---64bytes,目前仅支持GB2312
 */
static void game_main_send_fault(uint16_t fault_num, uint8_t *fault_str)
{
    uint8_t len = 0;

    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = 2;

    game_main_send_buff[1] = ((fault_num >> 8) & 0xff);
    game_main_send_buff[2] = (fault_num & 0xff);

    len = strlen((char *)fault_str);
    if(len >= (GAME_MAIN_FAULT_STRING_SIZE - 1))
    {
        len = (GAME_MAIN_FAULT_STRING_SIZE - 1);
    }
    memset((game_main_send_buff + 3), 0, GAME_MAIN_FAULT_STRING_SIZE);
    memcpy((game_main_send_buff + 3), fault_str, len);

    CMD_send_fault(game_main_send_buff, 67);
}

//
//
//
/**
 * @brief 设备注册-回调函数
 * 
 * @param result 结果
 * @param key_num 通讯key---string
 * @param device_id 设备id---string
 */
static void game_main_uart_device_register_callback(const uint8_t result, 
                                                    const uint8_t *key_num, 
                                                    const uint8_t *device_id)
{
    printf("device_register_callback\r\n");

    printf("result = %d\r\n", result);

    if(result == 0)
    {
        printf("result = %d\r\n", result);

        memcpy(game_main_config.device_register_key, key_num, sizeof(game_main_config.device_register_key));
        memcpy(game_main_config.device_register_device_num, device_id, sizeof(game_main_config.device_register_device_num));
        
        printf("device_register_key = %s\r\n", game_main_config.device_register_key);
        printf("device_register_device_num = %s\r\n", game_main_config.device_register_device_num);
    }
}

/**
 * @brief 设备注册-发送,,,配合游志云开发者平台,只需在出厂注册成功一次即可.
 * @param  
 */
static void game_main_send_device_register(void)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

	game_main_send_buff[0] = ((DEVICE_PRODUCT_ID >> 24) & 0xFF);
	game_main_send_buff[1] = ((DEVICE_PRODUCT_ID >> 16) & 0xFF);
	game_main_send_buff[2] = ((DEVICE_PRODUCT_ID >> 8) & 0xFF);
	game_main_send_buff[3] = (DEVICE_PRODUCT_ID & 0xFF);

	memcpy((game_main_send_buff + 4), device_id_buff_send, sizeof(device_id_buff_send));

	game_main_send_buff[20] = (HardVer >> 8) & 0xFF;
	game_main_send_buff[21] = HardVer & 0xFF;
	game_main_send_buff[22] = (SoftVer >> 24) & 0xFF;
	game_main_send_buff[23] = (SoftVer >> 16) & 0xFF;
	game_main_send_buff[24] = (SoftVer >> 8) & 0xFF;
	game_main_send_buff[25] = SoftVer & 0xFF;
	
    memcpy((game_main_send_buff + 26), APP_NAME, sizeof(APP_NAME));
	
    memcpy((game_main_send_buff + 58), APP_TITLE, sizeof(APP_TITLE));

	game_main_send_buff[90] = 1;

    CMD_send_device_register(game_main_send_buff, 91);
}

//
//
//
/**
 * @brief 带概率干预云支付---仅支持会员游玩
 * 
 * @param trans_id 事物id
 * @param play_times 游玩次数
 * @param lucky_arry 出奖数组---有play_times个数据
 * @param member_id 会员id(36 bytes) + 店铺id(8 bytes)
 */
static void game_main_uart_lucky_callback(const uint16_t trans_id, const uint8_t play_times, 
                                        const uint8_t *lucky_arry, const uint8_t *member_id)
{
    uint8_t str[37] = {0};
    uint8_t i;

    printf("lucky_callback\r\n");

    printf("trans_id = %d\r\n", trans_id);

    game_main_config.lucky_play_times = play_times;
    printf("lucky_play_times = %d\r\n", game_main_config.lucky_play_times);

    memset(game_main_config.lucky_arry, 0, sizeof(game_main_config.lucky_arry));
    memcpy(game_main_config.lucky_arry, lucky_arry, game_main_config.lucky_play_times);
    
    for (i = 0; i < game_main_config.lucky_play_times; i++)
    {
        printf("lucky_arry[%d] = %d\r\n", i, game_main_config.lucky_arry[i]);
    }
    
    memset(game_main_config.member_id, 0, sizeof(game_main_config.member_id));
    memcpy(game_main_config.member_id, member_id, sizeof(game_main_config.member_id));
    memcpy(str, member_id, sizeof(str));
    
    printf("member_id = %s\r\n", game_main_config.member_id);
    printf("member_id = %s\r\n", str);

    game_main_send_lucky(trans_id);
}

/**
 * @brief 带概率干预云支付-回应
 * @param trans_id 事物id
 */
static void game_main_send_lucky(uint16_t trans_id)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((trans_id >> 8) & 0xff);
    game_main_send_buff[1] = (trans_id & 0xff);

    game_main_send_buff[2] = 0;
    game_main_send_buff[3] = 0;

    CMD_send_lucky_rep(game_main_send_buff, 4);
}

//
//
//
/**
 * @brief 菜单项设置-回调函数
 * 
 * @param num 菜单编号
 * @param menu_type 菜单类型---1枚举,2数值,3字符串
 * @param menu_value 数值, 数值/枚举:传数值; 字符串:长度
 * @param menu_str 如果menu_type==3,字符串内容,其他NULL
 */
static void game_main_uart_menu_set_callback(const uint16_t num, const uint8_t menu_type, 
                                            const uint32_t menu_value, const uint8_t *menu_str)
{
    printf("menu_set_callback\r\n");

    game_main_config.menu_set_number = num;
    printf("num = %d\r\n", num);
    printf("menu_set_number = %d\r\n", game_main_config.menu_set_number);

    game_main_config.menu_set_type = menu_type;
    printf("menu_type = %d\r\n", menu_type);
    printf("menu_set_type = %d\r\n", game_main_config.menu_set_type);

    game_main_config.menu_set_value = menu_value;
    printf("menu_value = %d\r\n", menu_value);
    printf("menu_set_value = %d\r\n", game_main_config.menu_set_value);

    if(game_main_config.menu_set_type == MENU_TYPE_STRING)
    {
        memcpy(game_main_config.menu_set_str, menu_str, game_main_config.menu_set_value);
    }
    printf("menu_str = %s\r\n", menu_str);
    printf("menu_set_str = %s\r\n", game_main_config.menu_set_str);

    game_main_send_menu_set(0);
}

/**
 * @brief 菜单项设置-回应
 * @param resul 结果
 */
static void game_main_send_menu_set(const uint16_t resul)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((resul >> 8) & 0xFF);
    game_main_send_buff[1] = resul & 0xFF;
    
    CMD_send_menu_set_rep(game_main_send_buff, 2);
}

//
//
//
/**
 * @brief 菜单项读取-回调函数
 * 
 * @param num 菜单项编号
 */
static void game_main_uart_menu_read_callback(const uint16_t num)
{
    printf("menu_read_callback\r\n");

    game_main_config.menu_read_number = num;
    printf("num = %d\r\n", num);
    printf("menu_read_number = %d\r\n", game_main_config.menu_read_number);

    // MENU_TYPE_STRING
    // game_main_config.menu_read_flag = 1;

    // test
    if(game_main_config.menu_read_number == 3)
    {
        game_main_send_menu_read(0, 
                                game_main_config.menu_read_number, 
                                GAME_MAIN_MENU_TYPE_STRING, 
                                10,
                                "177643259");
    }
    else
    {
        game_main_send_menu_read(0, 
                                game_main_config.menu_read_number, 
                                GAME_MAIN_MENU_TYPE_VALUE, 
                                game_main_trans_id_get(),
                                NULL);
    }
}

/**
 * @brief 菜单项读取-回应
 * 
 * @param num 菜单编号
 * @param menu_type 菜单类型---1枚举,2数值,3字符串
 * @param menu_value 数值, 数值/枚举:传数值; 字符串:长度
 * @param menu_str 如果menu_type==3,字符串内容,其他为NULL.
 */
static void game_main_send_menu_read(const uint16_t state, 
                                    const uint16_t num, 
                                    const uint8_t menu_type, 
                                    const uint32_t menu_value, 
                                    const uint8_t *menu_str)
{
    uint16_t send_srt_len = 0;

    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));

    game_main_send_buff[0] = ((state >> 8) & 0xFF);
    game_main_send_buff[1] = (state & 0xFF);

    game_main_send_buff[2] = ((num >> 8) & 0xFF);
    game_main_send_buff[3] = (num & 0xFF);

    game_main_send_buff[4] = menu_type;

    game_main_send_buff[5] = ((menu_value >> 24) & 0xFF);
    game_main_send_buff[6] = ((menu_value >> 16) & 0xFF);
    game_main_send_buff[7] = ((menu_value >> 8) & 0xFF);
    game_main_send_buff[8] = (menu_value & 0xFF);

    if(menu_type == GAME_MAIN_MENU_TYPE_STRING)
    {
        memcpy((game_main_send_buff + 9), menu_str, menu_value);

        send_srt_len = 9 + menu_value;
    }
    else
    {
        send_srt_len = 9;
    }
    CMD_send_menu_read_rep(game_main_send_buff, send_srt_len);
}

//
//
//
/**
 * @brief 获取支付盒子信息-注册回调
 * 
 * @param result 结果
 * @param box_nmber 盒子编号---string
 * @param net_state 联网状态
 * @param login_state 登陆状态
 * @param shop_name 门店名称---string
 */
static void game_main_uart_box_info_callback(const uint16_t result, const uint8_t *box_nmber, 
										    const uint8_t net_state, const uint8_t login_state, 
                                            const uint8_t *shop_name)
{
    uint8_t str[37];

    printf("box_info_callback\r\n");

    printf("result = %d\r\n", result);

    game_main_config.net_state = net_state;
    printf("net_state = %d\r\n", net_state);

    game_main_config.login_state = login_state;
    printf("login_state = %d\r\n", login_state);

    if(game_main_config.net_state == 0)
    {
        memcpy(game_main_config.box_number, box_nmber, GAME_MAIN_BOX_NUMBER_SIZE);

        memset(str, 0, sizeof(str));
        memcpy(str, game_main_config.box_number, GAME_MAIN_BOX_NUMBER_SIZE);
    
        printf("box_number = %s\r\n", str);
    }

    if(game_main_config.login_state == 0)
    {
        memcpy(game_main_config.shop_name, shop_name, GAME_MAIN_SHOP_NAME_SIZE);

        memset(str, 0, sizeof(str));
        memcpy(str, game_main_config.shop_name, GAME_MAIN_SHOP_NAME_SIZE);
    
        printf("shop_name = %s\r\n", str);

        game_main_config.get_box_info_flag = GAME_MAIN_GET_BOX_INFO_SEND_FLAG_IDLE;
        game_main_config.pay_code_flag = GAME_MAIN_PAY_CODE_SEND_FLAG_RUN;
    }
}

/**
 * @brief 获取支付盒子信息-回应
 * @param  
 */
static void game_main_send_box_info(void)
{
    printf("game_main_send_box_info\r\n");

    CMD_send_box_info();
}

//
//
//
/**
 * @brief 获取消费码-注册回调函数
 * 
 * @param result 结果
 * @param pay_code 消费码---string
 */
static void game_main_uart_pay_code_callback(const uint16_t result, const uint8_t *pay_code)
{
    uint8_t str[129];

    printf("pay_code_callback\r\n");

    printf("result = %d\r\n", result);

    if(result == 0)
    {
        memcpy(game_main_config.pay_code, pay_code, GAME_MAIN_PAY_CODE_SIZE);

        memset(str, 0, sizeof(str));
        memcpy(str, game_main_config.pay_code, GAME_MAIN_PAY_CODE_SIZE);

        printf("pay_code = %s\r\n", str);
        
        game_main_config.pay_code_flag = GAME_MAIN_PAY_CODE_SEND_FLAG_IDLE;
    }
}

/**
 * @brief 获取消费码-回应
 * @param  
 */
static void game_main_send_pay_code(void)
{
    CMD_send_pay_code();
}

//
//
//
/**
 * @brief 
 * 
 * @param update_flag 
 * @param soft_ver 
 * @param internet_state 
 * @param addr_len 
 * @param data 
 */
static void game_main_uart_update_check_callback(const uint8_t update_flag, 
                                                const uint32_t soft_ver, 
                                                const uint8_t internet_state, 
                                                const uint8_t addr_len, 
                                                const uint8_t *data)
{
    printf("update_check_callback\r\n");

    printf("update_flag = %d\r\n", update_flag);
    printf("soft_ver = %d\r\n", soft_ver);
    printf("internet_state = %d\r\n", internet_state);
    printf("addr_len = %d\r\n", addr_len);
    printf("data = %s\r\n", data);
}

/**
 * @brief 获取消费码-回应
 * @param  
 */
static void game_main_send_update_check(void)
{
    uint16_t len = 0;

    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));
    
    game_main_send_buff[0] = ((HardVer >> 8) & 0xFF);
    game_main_send_buff[1] = (HardVer & 0xFF);
    
    game_main_send_buff[2] = ((SoftVer >> 24) & 0xFF);
    game_main_send_buff[3] = ((SoftVer >> 16) & 0xFF);
    game_main_send_buff[4] = ((SoftVer >> 8) & 0xFF);
    game_main_send_buff[5] = (SoftVer & 0xFF);
    
    len = sizeof(APP_NAME);
    if(len > 32)
    {
        len = 32;
    }
    memcpy((game_main_send_buff + 6), APP_NAME, len);
    
    len = sizeof(APP_TITLE);
    if(len > 32)
    {
        len = 32;
    }
    memcpy((game_main_send_buff + 38), APP_TITLE, len);
    
    len = sizeof(APP_DEVICE_TYPE);
    if(len > 32)
    {
        len = 32;
    }
    memcpy((game_main_send_buff + 70), APP_DEVICE_TYPE, len);

    CMD_send_update_check(game_main_send_buff, 102);
}

//
//
//
/**
 * @brief 
 * 
 * @param update_flag 
 * @param soft_ver 
 * @param internet_state 
 * @param addr_len 
 * @param data 
 */
static void game_main_uart_update_get_data_callback(const uint32_t offset, 
                                                const uint32_t data_size,
                                                const uint8_t *data)
{
    printf("update_get_data_callback\r\n");

    printf("update_flag = %d\r\n", offset);
    printf("soft_ver = %d\r\n", data_size);
    printf("data = %s\r\n", data);
}

/**
 * @brief 获取消费码-回应
 * @param  
 */
static void game_main_send_update_get_data(const uint32_t offset,
                                            const uint32_t data_size)
{
    memset(game_main_send_buff, 0, sizeof(game_main_send_buff));
    
    game_main_send_buff[0] = ((offset >> 24) & 0xFF);
    game_main_send_buff[1] = ((offset >> 16) & 0xFF);
    game_main_send_buff[2] = ((offset >> 8) & 0xFF);
    game_main_send_buff[3] = (offset & 0xFF);
    
    game_main_send_buff[4] = ((data_size >> 24) & 0xFF);
    game_main_send_buff[5] = ((data_size >> 16) & 0xFF);
    game_main_send_buff[6] = ((data_size >> 8) & 0xFF);
    game_main_send_buff[7] = (data_size & 0xFF);

    CMD_send_update_get_data(game_main_send_buff, 8);
}


