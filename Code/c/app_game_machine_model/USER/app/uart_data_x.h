
#ifndef _UART_DATA_XCODE_H_
#define _UART_DATA_XCODE_H_

#include "stm32f10x_conf.h"
#include "main.h"
#include "usart1_DMA.h"

// #define UART_DATA_XCODE_TIME_MAX	(10)	// 10ms
#define UART_DATA_XCODE_TIME_MAX	(5)	// 5ms

typedef struct 
{
	uint8_t data_get[USART1_DMA_RX_LEN];
	uint16_t data_get_len;
	uint8_t decode_time;
	uint8_t decode_flag;

}uart_data_x_t;

void uart_data_x_init(void);

// ------------------------
// ------------------------
// business data
typedef void (*CMD_connet_callback_t)(const uint32_t product_id, const uint8_t *device_id, const uint16_t devoce_type);

void CMD_connect_init(const uint32_t connect_version, 
					const uint32_t product_id, 
					const uint8_t *device_id, 
					const uint8_t p_all, 
					const uint8_t p_use, 
					const uint16_t device_type,
					const uint16_t soft_version,
					const CMD_connet_callback_t cb);
void CMD_connect_send(void);

typedef void (*CMD_state_callback_t)(const uint16_t device_state, 
									const uint8_t signal_value, 
									const uint8_t internet_state);
void CMD_state_callback_register(CMD_state_callback_t cb);
void CMD_send_status(uint8_t *buff, uint8_t len);

typedef void (*CMD_coin_in_callback_t)(const uint16_t trans_id, const uint16_t coin_num, const uint8_t coin_type, 
									const uint8_t *member_id, const uint8_t *member_name);
void CMD_coin_in_callback_register(CMD_coin_in_callback_t cb);
void CMD_send_coin_in_rep(uint8_t *buff, uint8_t len);

typedef void (*CMD_universal_config_get_callback_t)(void);
void CMD_universal_config_get_callback_register(CMD_universal_config_get_callback_t cb);
void CMD_send_universal_config_get_rep(uint8_t *buff, uint8_t len);

typedef void (*CMD_account_callback_t)(void);
void CMD_account_callback_register(CMD_account_callback_t cb);
void CMD_send_account_rep(uint8_t *buff, uint8_t len);

typedef void (*CMD_coin_phy_callback_t)(const uint16_t trans_id);
void CMD_coin_phy_callback_register(CMD_coin_phy_callback_t cb);
void CMD_send_coin_phy(uint8_t *buff, uint8_t len);

typedef void (*CMD_game_over_callback_t)(const uint16_t trans_id, const uint16_t result);
void CMD_game_over_callback_register(CMD_game_over_callback_t cb);
void CMD_send_game_over(uint8_t *buff, uint8_t len);

typedef void (*CMD_fault_callback_t)(const uint16_t result);
void CMD_fault_callback_register(CMD_fault_callback_t cb);
void CMD_send_fault(uint8_t *buff, uint8_t len);

typedef void (*CMD_device_register_callback_t)(const uint8_t result, const uint8_t *key_num, const uint8_t *device_id);
void CMD_device_register_callback_register(CMD_device_register_callback_t cb);
void CMD_send_device_register(uint8_t *buff, uint8_t len);

typedef void (*CMD_lucky_callback_t)(const uint16_t trans_id, 
									const uint8_t play_times, 
									const uint8_t *lucky_arry, 
									const uint8_t *member_id);
void CMD_lucky_callback_register(CMD_lucky_callback_t cb);
void CMD_send_lucky_rep(uint8_t *buff, uint8_t len);

typedef void (*CMD_menu_set_callback_t)(const uint16_t number_id, 
										const uint8_t menu_type, 
										const uint32_t menu_value, 
										const uint8_t *menu_str);
void CMD_menu_set_callback_register(CMD_menu_set_callback_t cb);
void CMD_send_menu_set_rep(uint8_t *buff, uint8_t len);

typedef void (*CMD_menu_read_callback_t)(const uint16_t number_id);
void CMD_menu_read_callback_register(CMD_menu_read_callback_t cb);
void CMD_send_menu_read_rep(uint8_t *buff, uint8_t len);

typedef void (*CMD_box_info_callback_t)(const uint16_t result, const uint8_t *box_nmber, 
										const uint8_t net_state, const uint8_t login_state, 
										const uint8_t *shop_name);
void CMD_box_info_callback_register(CMD_box_info_callback_t cb);
void CMD_send_box_info(void);

typedef void (*CMD_pay_code_callback_t)(const uint16_t result, const uint8_t *pay_code);
void CMD_pay_code_callback_register(CMD_pay_code_callback_t cb);
void CMD_send_pay_code(void);

typedef void (*CMD_update_check_callback_t)(const uint8_t update_flag, 
											const uint32_t soft_ver, 
											const uint8_t internet_state, 
											const uint8_t addr_len, 
											const uint8_t *data);
void CMD_update_check_callback_register(CMD_update_check_callback_t cb);
void CMD_send_update_check(uint8_t *buff, uint8_t len);

typedef void (*CMD_update_get_data_callback_t)(const uint32_t offset, const uint32_t data_size, const uint8_t *data);
void CMD_update_get_data_callback_register(CMD_update_get_data_callback_t cb);
void CMD_send_update_get_data(uint8_t *buff, uint8_t len);

#endif // _UART_DATA_XCODE_H_
