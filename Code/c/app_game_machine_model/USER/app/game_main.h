
#ifndef _GAME_MAIN_H_
#define _GAME_MAIN_H_

#include "stm32f10x_conf.h"

#define GAME_MAIN_STA_IDLE              (0)
#define GAME_MAIN_STA_RUN               (1)

//
//
//
// #define GAME_MAIN_TRANS_ID_MAX  (4294967294)
#define GAME_MAIN_NUMBER_ID_SIZE    (36)
#define GAME_MAIN_NUMBER_NAME_SIZE  (10)
#define GAME_MAIN_STORE_SIZE        (8)

#define GAME_MAIN_COIN_PHY_SEND_FLAG_IDLE       (0)
#define GAME_MAIN_COIN_PHY_SEND_FLAG_RUN        (1)
#define GAME_MAIN_COIN_PHY_SEND_FLAG_TRY        (2)
#define GAME_MAIN_COIN_PHY_SEND_DELAY_TIME_MAX  (50)

#define GAME_MAIN_DEVICE_REGISTER_GET_KEY_NUM           (32)
#define GAME_MAIN_DEVICE_REGISTER_GET_DEVICE_NUM_NUM    (9)

#define GAME_MAIN_LUCKY_ARRY_NUM                        (100)
// #define GAME_MAIN_MENU_SET_STRING_SIZE                  (128)
#define GAME_MAIN_MENU_SET_STRING_SIZE                  (64)
#define GAME_MAIN_SHOP_NAME_SIZE                        (36)

#define GAME_MAIN_GET_BOX_INFO_SEND_FLAG_IDLE   (0)
#define GAME_MAIN_GET_BOX_INFO_SEND_FLAG_RUN    (1)
#define GAME_MAIN_GET_BOX_INFO_SEND_FLAG_TRY    (2)
#define GAME_MAIN_BOX_NUMBER_SIZE               (10)
#define GAME_MAIN_GET_BOX_INFO_TIME_MAX         (100)

#define GAME_MAIN_PAY_CODE_SEND_FLAG_IDLE   (0)
#define GAME_MAIN_PAY_CODE_SEND_FLAG_RUN    (1)
#define GAME_MAIN_PAY_CODE_SEND_FLAG_TRY    (2)
#define GAME_MAIN_PAY_CODE_SIZE             (128)
#define GAME_MAIN_PAY_CODE_TIME_MAX         (100)

#define GAME_MAIN_OUT_TYPE_SCORE        (0)
#define GAME_MAIN_OUT_TYPE_TICKET_PHY   (1)
#define GAME_MAIN_OUT_TYPE_GIFT         (2)
#define GAME_MAIN_OUT_TYPE_COIN_PHY     (3)
#define GAME_MAIN_OUT_TYPE_TICKET_ELE   (4)
#define GAME_MAIN_OUT_TYPE_COIN_ELE     (5)

#define GAME_MAIN_FAULT_STRING_SIZE     (64)

#define GAME_MAIN_MENU_TYPE_ENUM        (1)
#define GAME_MAIN_MENU_TYPE_VALUE       (2)
#define GAME_MAIN_MENU_TYPE_STRING      (3)

#define GAME_MAIN_GAME_OVER_TRY_TIMES_MAX       (10)
#define GAME_MAIN_GAME_OVER_TRY_TIMES_TIME_MAX  (5)

//
//
//
typedef struct
{
    uint8_t state;

    uint16_t trans_id;

    uint16_t coin_residue;
    uint16_t over_time;
    uint16_t device_state;

    uint16_t coin_counts;
    uint8_t coin_type;
    char member_id[GAME_MAIN_NUMBER_ID_SIZE];
    char store_id[GAME_MAIN_STORE_SIZE];
    char member_name[GAME_MAIN_NUMBER_NAME_SIZE];

    uint8_t pay_game_coins; // ÿ�ֱ���

    uint32_t acc_coin_phy_num_all;
    uint32_t acc_coin_ele_num_all;
    uint32_t acc_gift_num_all;
    uint32_t acc_ticket_num_all;
    
    // send coin_phy
    uint32_t coin_phy_num;
    uint32_t coin_phy_send_num;
    uint16_t coin_phy_trans_id_send;
    uint8_t coin_phy_send_flag;
    uint8_t coin_phy_send_delay_time;

    // game over
    uint32_t gift_num;
    uint32_t gift_num_send;

    uint16_t game_over_flag;
    uint16_t game_over_trans_id_send;
    uint8_t game_over_try_times;
    uint16_t game_over_try_times_time;
    uint8_t game_over_out_type;

    // device register
    uint8_t device_register_key[GAME_MAIN_DEVICE_REGISTER_GET_KEY_NUM];
    uint8_t device_register_device_num[GAME_MAIN_DEVICE_REGISTER_GET_DEVICE_NUM_NUM];

    // lucky
    uint8_t lucky_play_times;
    uint16_t lucky_arry[GAME_MAIN_LUCKY_ARRY_NUM];

    // menu
    uint16_t menu_read_number;

    uint16_t menu_set_number;
    uint8_t menu_set_type;
    uint32_t menu_set_value;
    uint8_t menu_set_str[GAME_MAIN_MENU_SET_STRING_SIZE];

    // box info
    uint8_t get_box_info_flag;
    uint8_t get_box_info_time;
    uint8_t net_state;
    uint8_t login_state;
    uint8_t box_number[GAME_MAIN_BOX_NUMBER_SIZE];
    uint8_t shop_name[GAME_MAIN_SHOP_NAME_SIZE];

    // pay code
    uint8_t pay_code_flag;
    uint8_t pay_code_time;
    uint8_t pay_code[GAME_MAIN_PAY_CODE_SIZE];
    
}game_main_t;

extern game_main_t game_main_config;


void game_main_init(void);
void game_main_run(void);
void game_main_change_sta(uint8_t sta);

void game_main_time_check_1Ms(void);

void game_main_send_status(void);

#endif  //  _GAME_MAIN_H_
