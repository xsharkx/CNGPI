
/**
 * @file main.c
 * @author Model_Hui (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-02-16
 * 
 * @copyright Copyright (c) 2023  YouCaiHua Information Technology Co., Ltd
 * 
 */

#include "main.h"
#include <stdio.h>

#include "usart1_dma.h"
#include "timer2.h"
#include "led_run.h"
#include "device_id.h"
#include "key.h"

#include "game_main.h"

//
//
//
//---------------------------------------------------
void data_write_reverse(void *destin, void *source, uint16_t len)
{
	uint16_t i;
	if (source == NULL || destin == NULL)
	{
		return;
	}

	for (i = 0; i < len; i++)
	{
		*((uint8_t *)destin + i) = *((uint8_t *)source + len - 1 - i);
	}
}
//---------------------------------------------------

//
//
//
//---------------------------------------------------
// xor
uint8_t main_data_xor(const uint8_t *data, uint16_t len)
{
	uint16_t i;
	uint8_t xor = 0;

	for (i = 0; i < len; i++)
	{
		xor ^= *(data + i);
	}

	return xor;
}
//---------------------------------------------------

static uint8_t main_led_run_state = 0;

int main(void)
{
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0);
    
    SystemInit();

    uart1_dma_init(38400);
    timer2_init(1000, 72);

    led_run_init();
    device_id_init();
    key_init();

    printf("-----hello world-----\r\n");
    

    game_main_init();

    while (1)
    {
        if(timer2_500ms_sta_get() > 0)
        {
            if(main_led_run_state == 0)
            {
                main_led_run_state = 1;
            }
            else
            {
                main_led_run_state = 0;
            }

            led_run_set(main_led_run_state);
        }
        
        game_main_run();
    
        uart1_dma_get_data_check();
    }
}
